<!DOCTYPE HTML>
<html lang="zh-CN">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<a class="logo" href="http://www.funplus.com" title="Fun Plus Website">
	<img src="http://assets.theresumator.com/customers/customer_20140130164055_DMJP0Z7ZRDXJIZRO/layout/logo_funplus.png">
</a>
<a class="back-to-site" href="http://www.funplus.com/">« Back to FunPlus Website</a>
<div class="header_logo">

</div>
<?php
$PhotoBroad_values = get_option('PhotoBroad_framework_values');
if ( !is_array( $PhotoBroad_values ) ) $PhotoBroad_values = array();
if( array_key_exists( 'general_custom_favicon' , $PhotoBroad_values) && !empty( $PhotoBroad_values['general_custom_favicon'] ) ){ ?>
<link rel="shortcut icon" href="<?php echo $PhotoBroad_values['general_custom_favicon'];  ?>" />
<?php } ?>
<?php wp_head(); ?>
<?php
if( array_key_exists( 'style_custom_css' , $PhotoBroad_values) && !empty( $PhotoBroad_values['style_custom_css'] ) ){
echo '<style type="text/css">'."\n";
echo stripslashes( $PhotoBroad_values['style_custom_css'] )."\n";
echo '</style>'."\n";
}
?>
</head>
<body <?php body_class(); ?>>
	<div class="top-bar"></div>
	<div class="header clearfix">
		<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php
				if( array_key_exists( 'general_text_logo' , $PhotoBroad_values ) && $PhotoBroad_values['general_text_logo'] == 'on' ){
					echo esc_attr( get_bloginfo( 'name', 'display' ) );
				} else {
					if ( !empty( $PhotoBroad_values['general_custom_logo'] ) ){
						echo '<img src="'.get_bloginfo("template_url").'/timthumb.php?src='.$PhotoBroad_values['general_custom_logo'].'&amp;q=100&amp;h=41" alt="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" />';
					} else {
			?>
				<img src="<?php echo get_bloginfo("template_url"); ?>/images/logo.png" width="157" height="41" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
			<?php
					}
				}
			?></a></h1>

		<div class="menu">
			<ul>
				<?php
					$html = '';
					$categories = get_categories();
					$categories = json_decode(json_encode($categories), true);
				    foreach($categories as $val){
						$cat_name = $val['cat_name'];
						$cat_id = $val['cat_ID'];
						$html .= '<li class="page_item page-item-2">';

						$posts_page = home_url( '/' );
						//$html .= '	<a class="" href="http://192.168.20.248:301/?cat='.$cat_id.'">'.$cat_name.'</a>';
						$html .= '	<a class="" href="'.$posts_page.'?cat='.$cat_id.'">'.$cat_name.'</a>';
						$html .= '</li>';
					}
					echo $html;
				?>
			</ul>
		</div>
		<div class="sch"><?php get_search_form(); ?></div>
	</div>
