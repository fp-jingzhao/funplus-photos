<link href="/wp-content/themes/funplus/css/photos.css" type="text/css" rel="stylesheet">
<script src="/wp-content/themes/funplus/js/photos.js" type="text/javascript"></script>
<div id="image_parent">
</div>
<?php
function getValue($subject, $grep){//从photos的配置文件获取配置信息
    $pattern = "#define *\( *['\"]".$grep."['\"], *['\"](.*?)['\"] *\);#i";
    preg_match($pattern, $subject, $match);
    return isset($match[1])?$match[1]:'';
}
$file_name = ABSPATH . 'photos/wp-config.php';//读取photos配置文件
$subject = file_get_contents($file_name);

$host = getValue($subject, 'DB_HOST');//通过匹配找到DB_HOST
$host = !empty($host)?$host:'localhost';

$user = getValue($subject, 'DB_USER');
$user = !empty($user)?$user:'fpg';

$password = getValue($subject, 'DB_PASSWORD');
$password = !empty($password)?$password:'fpg';

$db = getValue($subject, 'DB_NAME');
$db = !empty($db)?$db:'photos';

$pattern = '#table_prefix *=* [\'"](.*?)[\'"];#i';
preg_match($pattern, $subject, $match);
$table_prefix = isset($match[1])?$match[1]:'pho_';

$conn = mysql_connect($host,$user,$password);
$db = mysql_select_db($db, $conn);
$sql = 'select post_content,post_title,guid from '.$table_prefix.'posts where post_status="publish" and post_parent=0 order by post_date desc limit 20';
$result = mysql_query($sql);
$data = array();
while($row = mysql_fetch_assoc($result)){
    $data[] = $row;
}
mysql_close($conn);
?>
    <?php
        $image = array();
        $js = "<script type='text/javascript'>";
        $js .= "var image_arr = '';";
        $i = 0;
        foreach($data as $val) {
            $post_content = $val['post_content'];
            $pattern = "/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";
            preg_match_all($pattern, $post_content, $match, PREG_PATTERN_ORDER);//匹配出所有图片的src
            if (isset($match[1])) {
                foreach ($match[1] as $key => $url) {
                    $image[] = array(
                        'url' => $url,
                        'title' => $val['post_title'],
                        'href' => $val['guid']
                    );
                }
            }
        }
        $js .= 'image_arr = '.json_encode($image).';';
        $js .= '</script>';
        echo $js;
    ?>
<script type="text/javascript">
    var FLAG = true;//执行标志，true的时候执行，false时不执行
    window.onscroll = function(){//抓取混动事件
        var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
        var clientWidth = document.documentElement.clientWidth;
        if(scrollTop > 5800 && FLAG){
            FLAG = false;
            if(clientWidth >750){//不为手机端时
                transitionPic(2,5,8,image_arr);
                return false;
            }else{//手机端效果照片一行显示一张
                FLAG = false;
                var oParent = document.getElementById('image_parent');
                for(var i=0; i<image_arr.length; i++){
                    var oHref = document.createElement('a');
                    oHref.href = image_arr[i].href;
                    oHref.target = '_blank';
                    var oImg  = document.createElement('img');
                    oImg.src = image_arr[i].url;
                    oImg.title = image_arr[i].title;
                    oImg.width = '100%';
                    oHref.appendChild(oImg);
                    oParent.appendChild(oHref);
                }
                return false;
            }
        }
        if(scrollTop < 5800){//鼠标画出屏幕时，清空照片
            document.getElementById('image_parent').innerHTML="";
            FLAG = true;
        }
    };
</script>
