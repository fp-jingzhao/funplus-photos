<?php
/**
 * Template Name: About Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package funplus
 */

get_header(); ?>


			<?php while ( have_posts() ) : the_post(); ?>

				<div class="cd-fixed-bg cd-bg-about">
					<h1 class="vert"><?php the_title(); ?></h1>
				</div> <!-- cd-fixed-bg -->



				<div class="cd-scrolling-bg cd-bg-cloud skew-1 skew-section">
					<div class="cd-container vert blue-contain rev-skew">
						<div class="col col-6">
							<?php the_field('about_text'); ?>
							<!--<h2 class ="effects fade from-left">More Fun</h2>
							<p class="effects fade from-left-slow">FunPlus is a mobile social gaming company that puts fun first for players worldwide. The company’s signature game, Family Farm, is enjoyed by over 4 million players each day and growing. Founded in 2010, FunPlus employs over 200 people and is headquartered in Beijing, China, with offices in San Francisco, CA, and Vancouver, Canada.</p>-->
						</div>
						<div class="col col-6">
							<img class="effects fade from-right" src="<?php the_field('about_image'); ?>">
						</div>
					</div>
				</div> <!-- cd-fixed-bg -->

				<?php
				$attachment_id = get_field('break_1_img');
				$full = "full"; // (thumbnail, medium, large, full or custom size)
				$med_dev = "med-dev";
				$sm_dev = "sm-dev";
				$image_full = wp_get_attachment_image_src( $attachment_id, $full );
				$image_med = wp_get_attachment_image_src( $attachment_id, $med_dev );
				$image_sm = wp_get_attachment_image_src( $attachment_id, $sm_dev );
				?>
				<style>
				.cd-bg-foosball {
					background-image: url('<?php echo $image_full[0]; ?>')
				}
				@media all and (max-width: 960px){
					.cd-bg-foosball {
						background-image: url('<?php echo $image_med[0]; ?>')
					}

				}
				@media all and (max-width: 480px){
					.cd-bg-foosball {
						background-image: url('<?php echo $image_sm[0]; ?>')
					}
				}
				</style>


				<div class="cd-fixed-bg cd-bg-foosball">

				</div> <!-- cd-scrolling-bg -->

				<div class="cd-scrolling-bg cd-bg-team skew-section skew-neg-15">
					<div class="cd-container vert orange-contain rev-skew">
						<div class="col col-6">
							<img class="effects fade from-left" src="<?php the_field('team_image'); ?>">
						</div>
						<div class="col col-6">
							<?php the_field('team_text'); ?>
							<!--<h2 class="effects from-right">The Team</h2>
							<p class+"effects from-right-slow">Our creative, respectful and hands-on teams are dedicated to making innovative, engaging games. And we’re always looking for highly-talented and ambitious entrepreneurs to join us. Come work at something you already love to do: making life more fun with games.</p>
							<a class="effects from-right button white" href="#">Job Openings</a>-->
						</div>
					</div>
				</div> <!-- cd-fixed-bg -->

				<?php
				$attachment_id = get_field('break_2_img');
				$full = "full"; // (thumbnail, medium, large, full or custom size)
				$med_dev = "med-dev";
				$sm_dev = "sm-dev";
				$image_full = wp_get_attachment_image_src( $attachment_id, $full );
				$image_med = wp_get_attachment_image_src( $attachment_id, $med_dev );
				$image_sm = wp_get_attachment_image_src( $attachment_id, $sm_dev );
				?>
				<style>
				.cd-bg-about-img2 {
					background-image: url('<?php echo $image_full[0]; ?>')
				}
				@media all and (max-width: 960px){
					.cd-bg-about-img2 {
						background-image: url('<?php echo $image_med[0]; ?>')
					}

				}
				@media all and (max-width: 480px){
					.cd-bg-about-img2 {
						background-image: url('<?php echo $image_sm[0]; ?>')
					}
				}
				</style>

				<div class="cd-fixed-bg cd-bg-about-img2">

				</div> <!-- cd-scrolling-bg -->

				<div class="cd-fixed-bg cd-bg-about-plus skew-section skew-15">
					<div class="cd-container vert blue-contain rev-skew">
						<div class="container-full culture-head">
							<div class="col col-6 effects fade from-left">
								<?php the_field('culture_text'); ?>

							</div>
						</div>

						<div class="container-full">
							<?php the_field('culture_text_boxes'); ?>
						</div>
					</div>
				</div> <!-- cd-fixed-bg -->


				<?php
				$attachment_id = get_field('break_3_img');
				$full = "full"; // (thumbnail, medium, large, full or custom size)
				$med_dev = "med-dev";
				$sm_dev = "sm-dev";
				$image_full = wp_get_attachment_image_src( $attachment_id, $full );
				$image_med = wp_get_attachment_image_src( $attachment_id, $med_dev );
				$image_sm = wp_get_attachment_image_src( $attachment_id, $sm_dev );
				?>
				<style>
				.cd-bg-about-img3 {
					background-image: url('<?php echo $image_full[0]; ?>')
				}
				@media all and (max-width: 960px){
					.cd-bg-about-img3 {
						background-image: url('<?php echo $image_med[0]; ?>')
					}

				}
				@media all and (max-width: 480px){
					.cd-bg-about-img3 {
						background-image: url('<?php echo $image_sm[0]; ?>')
					}
				}
				</style>
				<div class="cd-fixed-bg cd-bg-about-img3">

				</div> <!-- cd-scrolling-bg -->


				<div class="cd-scrolling-bg cd-bg-about-vision skew-neg-1 skew-section">
					<div class="cd-container vert blue-contain rev-skew">
						<h2 class="effects fade from-left"><?php the_field('vision_title'); ?></h2>
						<?php the_field('vision_text_boxes'); ?>

						<div class="center">
							<?php if(ICL_LANGUAGE_CODE=='en'): ?>
								<a class="button white-btn" href="http://www.funplus.com/?page_id=146">Join FunPlus</a>
							<?php elseif(ICL_LANGUAGE_CODE=='zh-hans'): ?>
								<a class="button white-btn" href="http://www.funplus.com/?page_id=146?lang=zh-hans">加入FunPlus</a>
							<?php endif;?>

						</div>
					</div>
				</div>

				<div class="cd-scrolling-bg cd-bg-about-img4">
<!--					<div class="cd-container vert blue-contain rev-skew">-->
<!--						<h2 class="effects fade from-left">--><?php //the_field('event_title'); ?><!--</h2>-->
<!--					</div>-->
					<?php
						include(ABSPATH . '/wp-content/themes/funplus/funplus_photos.php');
//						include(ABSPATH . 'funplus_photos.php');
					?>
				</div>
			<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>