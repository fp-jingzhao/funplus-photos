<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'photos');

/** MySQL database username */
define('DB_USER', 'fpg');

/** MySQL database password */
define('DB_PASSWORD', 'fpg');

/** MySQL hostname */
define('DB_HOST', 'localhost');
define('CONCATENATE_SCRIPTS', false );
/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'FqUNWVya1-94i< :-?};=w+w9-!grF-$FT0Ctsn&eY+O{W-}89OF+W+YNr;=c /.');
define('SECURE_AUTH_KEY',  'P5r-X|t5C670+yZaYQsP&58Lp?NfkD!7bF#!7~4u]o4H|tYHp|zvsy/FsKr@u>!}');
define('LOGGED_IN_KEY',    '*+Odmp{/mlpL.h9s;ct!rf<aM@@#c|HyDhyIW(6`SHD`:iif.7WtW]ITk!&C+KBH');
define('NONCE_KEY',        'f(GkxPMM<~!&{uFKEnwLk<a0.+S`y77mc|r%.^Yv.`otr:nqbJadg@L~{J|#T<U ');
define('AUTH_SALT',        't$LZ#YnHKEtsQLvx-#%$1D8zO$]Gi_1?o=ihQB]/RSYW5MuEgJvw#Q|Ay.rVQ=it');
define('SECURE_AUTH_SALT', '+A^UOUekhb<i/UtQR{:-+k5dqIJU|-dX,UVn7k/fqD6n}8 BLwd?DwS[x,Yr-a(M');
define('LOGGED_IN_SALT',   'vKOmaN[V{  s>Cd+w)3B/|B4n,m5BjO:4ma~wJg> COmds)+0P48^g7`?fj?XE|}');
define('NONCE_SALT',       '0`*XltQTJ<~~QDz=VzN=|Qg<D)-N$Y>#rH:mL9|}] +isyR,.*Gi2+Ht9=^^EvFZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pho_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

